import unittest

from FreeTwitch.response_metadata import ResponseMetadata

class TestResponseMetadata(unittest.TestCase):

    def test_from_string(self):
        response = """Content-Type: api/response
Content-Length: 28"""
        response_metadata = ResponseMetadata.from_string(response)
        self.assertEqual(28, response_metadata.length)

