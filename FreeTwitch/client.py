import socket

class Client:
    timeout = 5
    coding = 'utf-8'
    buffer_size = 1

    def __init__(self, host, port, string_control='\n\n'):
        self.host = host
        self.port = port
        self.string_control = string_control

    def connect(self, expected_response=''):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(self.timeout)
        self.socket.connect((self.host, self.port))

        if expected_response:
            buffer_size = len(expected_response) + len(self.string_control)
            return self.getText(buffer_size)

    def getText(self, length=0):
        if length:
            if length < 1:
                raise ValueError('length must be geater than 0')
            return self._getFixedText(length)
        return self._getUnlimitedText()

    def _getFixedText(self, length):
        buffer_size = length - len(self.string_control)
        response = self.socket.recv(buffer_size).decode(self.coding)
        self.socket.recv(len(self.string_control))
        return response

    def _getUnlimitedText(self):
        buffer_text = ''
        while True:
            buffer_text += self.socket.recv(self.buffer_size).decode(self.coding)
            has_end = buffer_text.count(self.string_control)
            if has_end:
                return buffer_text.split(self.string_control)[0]

    def send(self, data):
        fixed_data = data + self.string_control
        self.socket.sendall(fixed_data.encode())

    def close(self):
        self.socket.close()
