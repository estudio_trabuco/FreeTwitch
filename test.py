from FreeTwitch.response_metadata import ResponseMetadata

response = """Content-Type: api/response
Content-Length: 28"""

response_metadata = ResponseMetadata.from_string(response)
assert response_metadata.length == 28
