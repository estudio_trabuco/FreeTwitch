# README
Estudio: Librería para jugar con Freeswitch.

## Detalles del protocolo de Freeswitch

+ 2 saltos de linea separan el envio del comando y la respuesta recibida.
+ Al conectarse al servidor se recibe: "Content-Type: auth/request"
+ comando para autenticarse: "auth"
+ respuesta de autenticacion correcta: "Content-Type: command/reply\nReply-Text: +OK accepted"

### Documentacion defree switch api
https://developer.signalwire.com/freeswitch/FreeSWITCH-Explained/Modules/mod_commands_1966741/

#### convencion de la documentacion
+ <> indica que el parámetro es obligatorio
+ | sindica ó o ór
+ [] opcional

## Ejercicios

### 2023-07-15
```
api originate user/1002 &echo()
```
```
originate user/1002 &echo() nose XML nombre 2342343
```

## Tests

Correr los tests
```sh
python -m unittest tests/response_metadata.py
```

