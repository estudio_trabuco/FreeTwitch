from FreeTwitch.freeswitch_client import FreeswitchClient
from dotenv import dotenv_values

config = dotenv_values(".env")
host = config['HOST']
port = int(config['PORT'])

client = FreeswitchClient(host, port)

correct_password = config['PASSWORD']
incorrect_password = 'la misma de su email'
try:
    client.authenticate(correct_password)
    #client.authenticate(incorrect_password)
except:
    client.close()
    raise


print(client.call_echo_to_line(1002, 'prueba', '234435345345'))
#print(client.uptime())
